
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include "source/TaskQueueTokenized.h"
#include "source/include/Task.h"
#include <functional>
#include <future>
#include <iostream>

namespace {

struct ProcessCounter {
    unsigned int exec = 0;
    unsigned int clean = 0;
};
}
class TQTestsFixture {
public:
    TQTestsFixture()
        : mTqTokenized(factory::create())
    {
    }
    ~TQTestsFixture() = default;

protected:
    std::unique_ptr<ITaskQueueTokenized> mTqTokenized;
};

TEST_CASE_METHOD(TQTestsFixture, "Push one task and execute it", "[TQ]")
{
    ProcessCounter counter;
    auto handler = mTqTokenized->createTaskHandler("handler_1");
    handler->add({ [&counter] {
                      std::cout << "do Something" << std::endl;
                      ++counter.exec;
                  },
                   [&counter] {
                       std::cout << "clean Something" << std::endl;
                       ++counter.clean;
                   } });

    REQUIRE(counter.exec == 0);
    REQUIRE(counter.clean == 0);
    mTqTokenized->process();
    REQUIRE(counter.exec == 1);
    REQUIRE(counter.clean == 1);
}

TEST_CASE_METHOD(TQTestsFixture, "Push two task and execute them", "[TQ]")
{
    ProcessCounter counterT1;
    ProcessCounter counterT2;
    auto handler = mTqTokenized->createTaskHandler("handler_1");

    handler->add({ [&counterT1] { ++counterT1.exec; },
                   [&counterT1] { ++counterT1.clean; } });

    handler->add({ [&counterT2] { ++counterT2.exec; },
                   [&counterT2] { ++counterT2.clean; } });

    mTqTokenized->process();
    REQUIRE(counterT1.exec == 1);
    REQUIRE(counterT1.clean == 1);

    REQUIRE(counterT2.exec == 1);
    REQUIRE(counterT2.clean == 1);
}

TEST_CASE_METHOD(TQTestsFixture, "Push two task and reset handler", "[TQ]")
{
    ProcessCounter counterT1;
    ProcessCounter counterT2;
    auto handler = mTqTokenized->createTaskHandler("handler_1");

    handler->add({ [&counterT1] { ++counterT1.exec; },
                   [&counterT1] { ++counterT1.clean; } });

    handler->add({ [&counterT2] { ++counterT2.exec; },
                   [&counterT2] { ++counterT2.clean; } });

    handler.reset();
    CHECK(counterT1.exec == 0);
    REQUIRE(counterT1.clean == 1);
    CHECK(counterT2.exec == 0);
    REQUIRE(counterT2.clean == 1);
}

TEST_CASE_METHOD(TQTestsFixture, "Push a task and reset TQ", "[TQ]")
{
    ProcessCounter counterT1;
    ProcessCounter counterT2;
    auto handler = mTqTokenized->createTaskHandler("handler_1");

    handler->add({ [&counterT1] { ++counterT1.exec; },
                   [&counterT1] { ++counterT1.clean; } });

    mTqTokenized.reset();
    CHECK(counterT1.exec == 0);
    CHECK(counterT1.clean == 1);

    auto taskId = handler->add({ []() {} });
    REQUIRE(taskId.has_value() == false);
}

TEST_CASE_METHOD(TQTestsFixture, "Two handler and reset one", "[TQ]")
{
    ProcessCounter counterT1;
    ProcessCounter counterT2;
    auto handler1 = mTqTokenized->createTaskHandler("handler_1");
    auto handler2 = mTqTokenized->createTaskHandler("handler_2");

    handler1->add({ [&counterT1] { ++counterT1.exec; },
                    [&counterT1] { ++counterT1.clean; } });

    handler2->add({ [&counterT2] { ++counterT2.exec; },
                    [&counterT2] { ++counterT2.clean; } });

    handler1.reset();
    CHECK(counterT1.exec == 0);
    REQUIRE(counterT1.clean == 1);

    mTqTokenized->process();

    CHECK(counterT1.exec == 0);
    REQUIRE(counterT1.clean == 1);

    CHECK(counterT2.exec == 1);
    REQUIRE(counterT2.clean == 1);
}

TEST_CASE_METHOD(TQTestsFixture, "Push parallel task", "[TQ]")
{
    std::promise<int> countPromise;
    std::future<int> count = countPromise.get_future();

    auto handler = mTqTokenized->createTaskHandler("handler_1");

    handler->add({ { [&countPromise]() {
                        std::cout << "From parallel lambda " << std::endl;
                        countPromise.set_value(1);
                    },
                     exp::execution::parallel } });

    count.wait();
    REQUIRE(count.get() == 1);
}
