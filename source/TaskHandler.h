#pragma once
#include "include/ITaskHandler.h"
#include "include/Task.h"
#include <functional>
#include <optional>

template <
    typename AddTaskType,
    typename CancelTaskType,
    typename ScheduleTaskType>
class TaskHandler : public ITaskHandler {

public:
    TaskHandler(
        AddTaskType addTask,
        CancelTaskType cancelTask,
        ScheduleTaskType scheduleTask)
        : mAddTask(std::move(addTask))
        , mCancelTask(std::move(cancelTask))
        , mScheduleTask(std::move(scheduleTask))
    {
    }

    ~TaskHandler() {}

    std::optional<TaskId> add(Task task) override { return mAddTask(task); }

    std::optional<TaskId> schedule(Task task, int count, int timeout) override
    {
        return mScheduleTask(task, count, timeout);
    }

    void cancel(TaskId task) override { mCancelTask(task); }

private:
    AddTaskType mAddTask;
    CancelTaskType mCancelTask;
    ScheduleTaskType mScheduleTask;
};
