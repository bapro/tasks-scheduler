#pragma once

#include "Handler.h"
#include "include/Task.h"
#include "TaskHandler.h"
#include <functional>
#include <map>
#include <mutex>
#include <vector>

// Basic impl of a queue just for testing
class Queue {
public:
    Queue() = default;
    ~Queue() = default;

    Handler createHandler(std::string_view handlerName);
    std::optional<TaskId> addTask(Handler handler, Task task);
    void cancelTask(Handler handler, TaskId taskId);
    void cancelTasksFromHandler(Handler handler);

    std::optional<TaskId>
    schedule(Handler handler, Task task, int count, int timeout);

    void process();

    void clear();

private:
    HandlerId mHandlerId = 0;
    TaskId mTaskId = 0;
    std::map<Handler, std::vector<std::pair<TaskId, Task>>> mTasks;
    std::mutex mTheLocker;
};
