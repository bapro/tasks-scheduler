#include "TaskQueueTokenized.h"
#include <iostream>

namespace factory {
std::unique_ptr<ITaskQueueTokenized> create()
{
    return std::make_unique<TaskQueueTokenized>();
}
}

namespace detail {
DeleterTaskHandler::DeleterTaskHandler(std::function<void()>&& deleter)
    : mDeleter(std::move(deleter))
{
}

void DeleterTaskHandler::operator()(ITaskHandler* ptr)
{
    mDeleter();
    delete ptr;
}
}

TaskQueueTokenized::TaskQueueTokenized()
    : mQueue(std::make_shared<Queue>())
{
}

TaskQueueTokenized::~TaskQueueTokenized() { mQueue->clear(); }

TaskHandlerPtr TaskQueueTokenized::createTaskHandler(
    std::string_view handlerName)
{

    auto hdlr = mQueue->createHandler(handlerName);
    std::weak_ptr<Queue> weakThis = mQueue;
    auto addFunction = [weakThis, hdlr](Task task) -> std::optional<TaskId> {
        if (auto sharedThis = weakThis.lock()) {
            return sharedThis->addTask(hdlr, task);
        }
        return {};
    };

    auto cancelFunction = [weakThis, hdlr](TaskId taskId) {
        if (auto sharedThis = weakThis.lock()) {
            return sharedThis->cancelTask(hdlr, taskId);
        }
    };

    auto scheduleFunction =
        [weakThis,
         hdlr](Task task, int count, int timeout) -> std::optional<TaskId> {
        if (auto sharedThis = weakThis.lock()) {
            return sharedThis->schedule(hdlr, task, count, timeout);
        }
        return {};
    };
    auto deleterFunction = [weakThis, hdlr] {
        if (auto sharedThis = weakThis.lock()) {
            sharedThis->cancelTasksFromHandler(hdlr);
        }
    };

    return TaskHandlerPtr(
        new TaskHandler(addFunction, cancelFunction, scheduleFunction),
        detail::DeleterTaskHandler(deleterFunction));
}
void TaskQueueTokenized::process() { mQueue->process(); }
