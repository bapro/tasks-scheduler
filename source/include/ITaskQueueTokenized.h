

#pragma once

#include "ITaskHandler.h"
#include <string_view>

class ITaskQueueTokenized {
public:
    virtual ~ITaskQueueTokenized() = default;

    virtual TaskHandlerPtr createTaskHandler(std::string_view handlerName) = 0;
    virtual void process() = 0;
};

namespace factory {
std::unique_ptr<ITaskQueueTokenized> create();
}
