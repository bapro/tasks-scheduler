

#pragma once
#include "function.h"

class Task {
public:
    Task(exp::function<void()> function)
        : mTask(function)
    {
    }

    Task(exp::function<void()> function, exp::function<void()> cleanup)
        : mTask(function)
        , mCleanup(cleanup)
    {
    }

    void execute() { mTask(); };
    void clean() { mCleanup(); };
    exp::execution policy() { return mTask.policy(); }

private:
    exp::function<void()> mTask;
    exp::function<void()> mCleanup;
};