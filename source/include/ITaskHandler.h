

#pragma once
#include "Task.h"
#include "TaskId.h"
#include <optional>

class ITaskHandler {
public:
    virtual ~ITaskHandler() = default;

    virtual std::optional<TaskId> add(Task task) = 0;

    virtual std::optional<TaskId>
    schedule(Task task, int count, int timeout) = 0;

    virtual void cancel(TaskId task) = 0;
};

namespace detail {
struct DeleterTaskHandler {
    DeleterTaskHandler(std::function<void()>&& deleter);
    void operator()(ITaskHandler* ptr);

private:
    std::function<void()> mDeleter;
};
}

using TaskHandlerPtr = std::
    unique_ptr<ITaskHandler, detail::DeleterTaskHandler>;