

#pragma once
#include <functional>

namespace exp {
enum class execution { sequence, parallel };

template <class Fp> class function;

template <class R, class... ArgTypes> class function<R(ArgTypes...)> {
public:
    function() = default;
    ~function() = default;

    function(const std::function<R(ArgTypes...)>& rhs)
        : mFunction(rhs)
    {
    }

    function(std::function<R(ArgTypes...)>&& rhs) noexcept
        : mFunction(std::move(rhs))
    {
    }

    template <class Fp>
    function(Fp rhs)
        : mFunction(std::move(rhs))
    {
    }

    template <class Fp>
    function(Fp rhs, const execution& policy)
        : mFunction(std::move(rhs))
        , mPolicy(policy)
    {
    }

    function(
        const std::function<R(ArgTypes...)>& rhs,
        const execution& policy)
        : mFunction(rhs)
        , mPolicy(policy)
    {
    }

    function(
        std::function<R(ArgTypes...)>&& rhs,
        const execution& policy) noexcept
        : mFunction(std::move(rhs))
        , mPolicy(policy)
    {
    }


    function& operator=(const std::function<R(ArgTypes...)>& rhs)
    {
        mFunction.swap(rhs);
        return *this;
    }
    function& operator=(std::function<R(ArgTypes...)>&& rhs) noexcept
    {
        mFunction = std::move(rhs);
        return *this;
    }

    template <class Fp> function& operator=(Fp&& rhs)
    {
        mFunction = std::move(rhs);
        return *this;
    }

    R operator()(ArgTypes...args) const
    {
        return mFunction(std::forward<args>(args)...);
    }

    execution policy() { return mPolicy; };

private:
    std::function<R(ArgTypes...)> mFunction;
    execution mPolicy = execution::sequence;
};

}