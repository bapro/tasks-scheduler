#include "Queue.h"
#include <future>
#include <iostream>

std::optional<TaskId> Queue::addTask(Handler handler, Task task)
{
    //Basic impl for the example
    if (task.policy() == exp::execution::parallel) {
        auto tkFuture = std::async(
            std::launch::async,
            [](Task tsk) {
                tsk.execute();
                tsk.clean();
            },
            task);
        return ++mTaskId;
    }
    std::scoped_lock<std::mutex> locker(mTheLocker);
    auto taskId = ++mTaskId;
    mTasks[handler].push_back({ taskId, task });
    return taskId;
}
void Queue::cancelTasksFromHandler(Handler handler)
{
    std::scoped_lock<std::mutex> locker(mTheLocker);
    for (auto& task : mTasks[handler]) {
        task.second.clean();
    }
    mTasks[handler].clear();
    mTasks.erase(handler);
}
void Queue::cancelTask(Handler handler, TaskId taskId)
{
    std::scoped_lock<std::mutex> locker(mTheLocker);
    auto itSearch = std::find_if(
        mTasks[handler].begin(), mTasks[handler].end(), [taskId](auto& task) {
            return task.first == taskId;
        });

    if (itSearch != mTasks[handler].end()) {
        itSearch->second.clean();
        mTasks[handler].erase(itSearch);
    }
}

std::optional<TaskId>
Queue::schedule(Handler handler, Task task, int count, int timeout)
{
    // TODO implement it
    return std::optional<TaskId>();
}

void Queue::process()
{
    std::scoped_lock<std::mutex> locker(mTheLocker);
    for (auto& [handler, tasks] : mTasks) {
        std::cout << "Processing handler id: " << handler.name;
        for (auto& task : tasks) {
            std::cout << " - Processing task id: " << task.first << std::endl;
            task.second.execute();
            task.second.clean();
        }
        tasks.clear();
    }
}
Handler Queue::createHandler(std::string_view handlerName)
{
    std::scoped_lock<std::mutex> locker(mTheLocker);

    auto handlerId = ++mHandlerId;
    Handler hdlr = { handlerId, std::string(handlerName) };
    mTasks[hdlr] = std::vector<std::pair<TaskId, Task>>();
    return hdlr;
}
void Queue::clear()
{

    std::scoped_lock<std::mutex> locker(mTheLocker);
    for (auto& [hdlr, tasks] : mTasks) {
        for (auto& task : mTasks[hdlr]) {
            task.second.clean();
        }
        mTasks[hdlr].clear();
    }
}
