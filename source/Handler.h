#pragma once
#include <string>

using HandlerId = unsigned int;

struct Handler {
    HandlerId id;
    std::string name;

    bool operator<(const Handler& rhs) const { return id < rhs.id; }
};