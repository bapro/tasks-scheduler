#pragma once

#include "Handler.h"
#include "Queue.h"
#include "TaskHandler.h"
#include "include/ITaskQueueTokenized.h"
#include "include/Task.h"
#include <functional>
#include <map>
#include <mutex>
#include <vector>

class TaskQueueTokenized : public ITaskQueueTokenized {
public:
    TaskQueueTokenized();
    ~TaskQueueTokenized();

    TaskHandlerPtr createTaskHandler(std::string_view handlerName) override;
    void process() override;

private:
    std::shared_ptr<Queue> mQueue;
};